#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    std::cout << buf.tm_mday << "\n";

    const int ArraySize = 5;
    int array[ArraySize][ArraySize];
    int SumOfLine = 0;
    int TimeDivision = buf.tm_mday % ArraySize;

    for (int i = 0; i < ArraySize; i++)
    {
        for (int j = 0; j < ArraySize; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }

        std::cout << "\n";
    }

    for (int i = 0; i < ArraySize; i++)
    {
        SumOfLine += array[TimeDivision][i];
    }

    std::cout << SumOfLine << "\n";
}

